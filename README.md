## About
This repo contains both client side and server side codes. To whom want to test this project need to run `npm i` in both root folder and server folder. And also need to start both project.

## About client side
I have used polling every 30s to get and update the currency prices.

## About server side
To prevent hitting API rate limit, I store the response to a json file (Should be database). And I have added a schedule event to query the updated prices every 30s.  
