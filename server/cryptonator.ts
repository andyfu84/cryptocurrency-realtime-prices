import fetch from "cross-fetch";

const BASE_URL = "https://api.cryptonator.com/api";

export type Ticker = {
  ticker?: {
    base: string;
    target: string;
    price: string;
    volume: string;
    change: string;
  },
  timestamp?: number;
  success: boolean;
  error: string;
}

export type Currency = {
  code: string;
  name: string;
  statuses: string[];
}

export type CurrencyResponse = {
  rows: Currency[]
}

export const getTicker = async (currency: string): Promise<Ticker> => {
  const response = await fetch(`${BASE_URL}/ticker/${currency}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json; charset=utf-8",
      Accept: "application/json; charset=utf-8",
    }
  });
  return (await response.json()) as Ticker;
};

export const getCurrencies = async (): Promise<Currency[]> => {
  const response = await fetch(`${BASE_URL}/currencies`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json; charset=utf-8",
      Accept: "application/json; charset=utf-8",
    }
  });

  const parsedResponse = (await response.json()) as CurrencyResponse;

  return parsedResponse.rows;
};
