import { readFileSync, writeFileSync } from "fs";
import express from "express";
import cors from "cors";
import { schedule } from "node-cron";
import pLimit from "p-limit";
import type { Currency, Ticker } from "./cryptonator";
import { getCurrencies, getTicker } from "./cryptonator";

const app = express();
const port = 3001;
const getTickerLimit = pLimit(1); // Throttle the get functions. Can be adjusted if knowing the rate limit
const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

app.use(cors());

const currencyPairs = ["btc-usd", "eth-usd", "ltc-usd", "xmr-usd", "xrp-usd", "doge-usd", "dash-usd", "maid-usd", "lsk-usd", "sjcx-usd"];
let tickers: Ticker[], currencies: Currency[];

const initialTickers = async () => {
  if (!tickers) {
    try {
      tickers = JSON.parse((await readFileSync("tickers.json")).toString());
    } catch (e) {
      console.log(e);
      tickers = [];
    }
  }
};

const initialCurrencies = async () => {
  if (!currencies) {
    try {
      currencies = JSON.parse((await readFileSync("currencies.json")).toString());
    } catch (e) {
      console.log(e.message);
      currencies = await getCurrencies();
      await writeFileSync("currencies.json", JSON.stringify(currencies));
    }
  }
};

const findTickerIndex = (currencyPair: string) => {
  const [base, target] = currencyPair.split("-");
  return tickers.findIndex(({
    ticker: {
      base: currentBase,
      target: currentTarget
    }
  }) => base.toUpperCase() === currentBase && target.toUpperCase() === currentTarget);
};

app.get("/", (req, res) => {
  res.send("Hello World.");
});

app.get("/tickers", async (req, res) => {
  await initialTickers();
  res.send(JSON.stringify(tickers));
});

app.get("/tickers/:currencyPair", async (req, res) => {
  await initialTickers();
  const { params: { currencyPair } } = req;
  if (currencyPairs.indexOf(currencyPair)) res.send();

  const index = findTickerIndex(currencyPair);
  if (index === null) {
    res.status(404).send(JSON.stringify({ error: "invalid currency pair." }));
  }
  res.send(tickers[index]);
});

schedule("*/30 * * * * *", async () => {
  await Promise.all([initialTickers(), initialCurrencies()]);
  await Promise.all(currencyPairs.map((currencyPair) => getTickerLimit(async () => {
    const ticker = await getTicker(currencyPair);
    if (ticker.success) {
      const index = findTickerIndex(currencyPair);
      const [base] = currencyPair.split("-");
      const currency = currencies.find(({ code }) => base.toUpperCase() === code);
      if (index === -1) {
        tickers.push({ ...currency, ...ticker });
      } else {
        if (ticker.timestamp > tickers[index].timestamp) {
          tickers[index] = { ...currency, ...ticker };
        }
      }
    }
    await sleep(500);
  })));

  console.log("Last update time:", Date().toString());
  await writeFileSync("tickers.json", JSON.stringify(tickers));
});

app.listen(port, () => {
  console.log(`Listening port: ${port}.`);
});
