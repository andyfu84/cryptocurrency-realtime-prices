import React, { useEffect, useState } from "react";
import "./App.css";

type PriceBlockParams = {
  name: string;
  ticker: {
    base: string;
    price: number;
    change: string;
    volume: number;
  }
}

const PriceBlock = ({ name, ticker: { price, volume, change } }: PriceBlockParams) => {
  return (
    <div className="col-4 mb-3">
      <div className="border-1 border-secondary rounded border p-2">
        <h2>{name}</h2>
        <div className="fw-bold text-warning">${price}</div>
        <div className="row">
          <div className="col-6">
            <div className="text-muted fw-bold">volume:</div>
            <div className="text-muted">{volume}</div>
          </div>
          <div className="col-6">
            <div className="text-muted fw-bold">change:</div>
            <div className={parseFloat(change) > 0 ? "text-success" : "text-danger"}>{change}</div>
          </div>
        </div>
      </div>
    </div>
  );
};

const App = () => {
  const [list, setList] = useState([]);

  useEffect(() => {
    const polling = () => {
      fetch(`http://localhost:3001/tickers`)
        .then((response) => response.json())
        .then((data) => setList(data));
      console.log("Last update time:", Date().toString());
    };

    const intervalId = setInterval(polling, 30000);
    polling();
    return () => clearInterval(intervalId);
  }, []);

  return (
    <div className="container py-5">
      <h3>Cryptocurrency Realtime Prices</h3>
      <div className="row">
        {list.map((item, index) => (<PriceBlock key={index} {...item} />))}
      </div>
    </div>
  );
};

export default App;
